# Java-based Spectrum Analyzer #

This application includes tools for analyzing radio or audio signals.

Other than using standard FFT code, all other algorithms were written 
without consulting other code sources or algorithms as an exercise in 
reverse engineer existing technologies.  Undoubtedly better implementations
of filters, demodulators, and other DSP functions can be found elsewhere - 
these implementations are merely for fun - but do work.

Everything in this project should be considered beta, but should serve as 
a starting point for those interested in digital signal processing. All
demodulate/decode functions assume a filtered signal.  Run the signal
through the band pass filter before trying to demodulate.

# Currently Working For #
* 8K 16-bit PCM wave files
* Line in (analysis only)

# Functionality Includes #
## Signal Analysis ##
* Spectrum Analyzer
* Waterfall Display
* Oscilloscope
## Demodulate/Decode ##
* RTTY
* PSK31
* DTMF
* CW
## DSP Functions ##
* Tone Generator
* Audio Mixer
* Gain Controller
* Band Pass Filter
* Frequency Translator

# Dependencies
This project uses [WaveUtils](https://bitbucket.org/tcgerlach/waveutils) for processing audio files. Please build and install this library before building this application.