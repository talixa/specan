package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.audio.riff.exceptions.RiffFormatException;
import com.talixa.specan.demod.psk.Psk31Demod;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameDemodPsk {

	private static final int WIDTH = 300;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * 2;
			
	public static void createAndShowGUI(final String inputFile) {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_DEMOD_PSK);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(frame);	
							
		// center freq panel
		JPanel freqPanel = new JPanel(new FlowLayout());
		freqPanel.add(new JLabel("Center Freq"));
		final JTextField freqText = new JTextField(10);
		freqPanel.add(freqText);		
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int center = Integer.parseInt(freqText.getText());
				
				try {
					Psk31Demod demod = new Psk31Demod(inputFile);
					String data = demod.demodulate(center);		
					data = data.replaceAll("\r", "\n");
					FrameDecodedMessage.createAndShowGUI(data);														
					frame.dispose();				
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(frame, SpecAnConstants.ERROR_FILE_LOAD);
				} catch (RiffFormatException e1) {
					JOptionPane.showMessageDialog(frame, SpecAnConstants.ERROR_BAD_WAVE);
				}
			}
		});
				
		// Add to frame
		frame.add(freqPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
