package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.talixa.specan.listeners.ExitActionListener;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameAbout {
	
	private static final int WIDTH = 500;
	private static final int HEIGHT = 270;
	
	private static final String ABOUT_CONTENTS = 
			"<html><center>A simple spectrum analyzer for audio files. Includes spectrum analyzer, waterfall, " +
			"and oscilloscope display. Currently works with 16-bit PCM files with an 8K sample rate. " +
		    "<br><br>Other functions include tone generator, audio mixer, bandpass filters, and demodulators " +
			"for PSK31 and RTTY. Demod functions are experimental. " +
			"<br><br>Version: " + SpecAnConstants.VERSION + 
			"<br>&copy;2017, Talixa Software & Service, LLC</center></html>";
	
	private static final int BORDER = 10;
		
	public static void createAndShowGUI(JFrame owner) {
		// Create dialog
		JDialog dialog = new JDialog(owner, SpecAnConstants.TITLE_ABOUT);
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(dialog);		
		
		// set escape listener
		addEscapeListener(dialog);
		
		// set modality
		dialog.setModal(true);
		
		// create a content holder with a 10 pixel border
		JPanel holder = new JPanel();
		holder.setLayout(new BorderLayout());
		holder.setBorder(BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER));
		dialog.add(holder);
	
		// create components
		ClassLoader cl = IconHelper.class.getClassLoader();	
		JLabel icon = new JLabel(new ImageIcon(cl.getResource(SpecAnConstants.ICON)));
		holder.add(icon, BorderLayout.WEST);
		
		JLabel text = new JLabel(ABOUT_CONTENTS);
		Border padding = BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER);
		text.setBorder(padding);
		JButton ok = new JButton(SpecAnConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(dialog));
				
		// Add to frame
		holder.add(text, BorderLayout.CENTER);
		holder.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		dialog.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		dialog.pack();
		dialog.setLocation(left,top);
		dialog.setVisible(true);						
	}	
	
	public static void addEscapeListener(final JDialog dialog) {
	    ActionListener escListener = new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            dialog.dispose();
	        }
	    };

	    dialog.getRootPane().registerKeyboardAction(escListener,
	            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
	            JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
}
