package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.talixa.audio.wav.WaveFile;
import com.talixa.specan.SpectrumAnalyzer;
import com.talixa.specan.dsp.FrequencyTranslator;
import com.talixa.specan.dsp.SharedDSPFunctions;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameFrequencyTranslator {

	private static final int ROW_COUNT = 3;
	private static final int WIDTH = SpecAnConstants.DEFAULT_FRAME_WIDTH;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * (ROW_COUNT+1);
	
	public static void createAndShowGUI(final String inputFile) {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_FREQUENCY_TRANSLATION);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(frame);	
		
		// panel outputfile
		JPanel outputDataPanel = new JPanel(new FlowLayout());
		final JLabel outputFileLabel = new JLabel(SpecAnConstants.NO_FILE_SELECTED);
		JButton selectOutputButton = new JButton(SpecAnConstants.SELECT_OUTPUT_FILE);
		selectOutputButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = SpectrumAnalyzer.fileChooser.showSaveDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					outputFileLabel.setText(SpectrumAnalyzer.fileChooser.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		outputDataPanel.add(selectOutputButton);
		outputDataPanel.add(outputFileLabel);
		
		// freq panel
		JPanel freqPanel = new JPanel(new FlowLayout());
		freqPanel.add(new JLabel("Shift Frequency"));
		final JTextField freqText = new JTextField(10);
		freqPanel.add(freqText);
		
		// direction panel
		JPanel directionPanel = new JPanel(new FlowLayout());
		JLabel directionLabel = new JLabel("Direction: ");
		final JRadioButton radioUp = new JRadioButton("Up");
		final JRadioButton radioDown = new JRadioButton("Down");
		ButtonGroup bg = new ButtonGroup();
		bg.add(radioDown);
		bg.add(radioUp);		
		bg.setSelected(radioDown.getModel(), true);
		directionPanel.add(directionLabel);
		directionPanel.add(radioDown);
		directionPanel.add(radioUp);	
		
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(ROW_COUNT, 1));
		mainPanel.add(outputDataPanel);
		mainPanel.add(freqPanel);
		mainPanel.add(directionPanel);
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {	
				int freq = Integer.valueOf(freqText.getText());
				
				try {
					WaveFile wave1 = SpectrumAnalyzer.readWaveFile(inputFile);		
					if (wave1 != null) {
						short[] inputData = SharedDSPFunctions.extractWaveFileData(wave1);
						String outputFile = outputFileLabel.getText();
						if (radioDown.isSelected()) {
							FrequencyTranslator.shiftDown(inputData, freq, outputFile);
						} else {
							FrequencyTranslator.shiftUp(inputData, freq, outputFile);
						}	
					}
				} catch (IOException e1) {						
					JOptionPane.showMessageDialog(frame, SpecAnConstants.ERROR_FILE_LOAD);
				} 				
				frame.dispose();															
			}
		});
				
		// Add to frame
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
