package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.specan.SpectrumAnalyzer;
import com.talixa.specan.demod.fsk.RttyDemod;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameDemodFsk {

	private static final int ROW_COUNT = 4;
	private static final int WIDTH = SpecAnConstants.DEFAULT_FRAME_WIDTH;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * (ROW_COUNT+1);
	
	private static final String DEFAULT_OUTPUT_LABEL = "Empty to decode baudot";
	
	public static void createAndShowGUI(final String inputFile) {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_DEMOD_FSK);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(frame);	
			
		// panel outputfile
		JPanel outputDataPanel = new JPanel(new FlowLayout());
		final JLabel outputFileLabel = new JLabel(DEFAULT_OUTPUT_LABEL);
		JButton selectOutputButton = new JButton(SpecAnConstants.SELECT_OUTPUT_FILE);
		selectOutputButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = SpectrumAnalyzer.fileChooser.showSaveDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					outputFileLabel.setText(SpectrumAnalyzer.fileChooser.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		outputDataPanel.add(selectOutputButton);
		outputDataPanel.add(outputFileLabel);
		
		// low freq panel
		JPanel lowFreqPanel = new JPanel(new FlowLayout());
		lowFreqPanel.add(new JLabel("Low Freq"));
		final JTextField lowFreqText = new JTextField(10);
		lowFreqPanel.add(lowFreqText);
		
		// high freq panel
		JPanel highFreqPanel = new JPanel(new FlowLayout());
		highFreqPanel.add(new JLabel("High Freq"));
		final JTextField highFreqText = new JTextField(10);
		highFreqPanel.add(highFreqText);
		
		// baud rate panel
		JPanel baudRatePanel = new JPanel(new FlowLayout());
		baudRatePanel.add(new JLabel("Baud Rate"));
		final JTextField baudRateText = new JTextField(10);
		baudRatePanel.add(baudRateText);
		
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(ROW_COUNT, 1));
		mainPanel.add(outputDataPanel);
		mainPanel.add(lowFreqPanel);
		mainPanel.add(highFreqPanel);
		mainPanel.add(baudRatePanel);
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int low = Integer.parseInt(lowFreqText.getText());
				int high = Integer.parseInt(highFreqText.getText());
				double baud = Double.parseDouble(baudRateText.getText());
				
				if (outputFileLabel.getText().equals(DEFAULT_OUTPUT_LABEL)) {
					RttyDemod demod = new RttyDemod(inputFile);
					String data = demod.demodulateBaudot(low,high,baud);
					FrameDecodedMessage.createAndShowGUI(data);
				} else {
					RttyDemod demod = new RttyDemod(inputFile, outputFileLabel.getText());	
					demod.demodulate(low, high, baud);
				}
												
				frame.dispose();
			}
		});
				
		// Add to frame
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
