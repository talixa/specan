package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.talixa.audio.wav.WaveFile;
import com.talixa.specan.SpectrumAnalyzer;
import com.talixa.specan.dsp.AudioMixer;
import com.talixa.specan.dsp.SharedDSPFunctions;
import com.talixa.specan.dsp.AudioMixer.MixerMode;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameMixer {

	private static final int ROW_COUNT = 4;
	private static final int WIDTH = SpecAnConstants.DEFAULT_FRAME_WIDTH;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * (ROW_COUNT+1);	
	
	public static void createAndShowGUI() {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_MIXER);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(frame);	
			
		// panel input1
		JPanel input1Panel = new JPanel(new FlowLayout());
		final JLabel inputLabel1 = new JLabel(SpecAnConstants.NO_FILE_SELECTED);
		JButton selectInput1 = new JButton(SpecAnConstants.SELECT_INPUT_FILE);
		selectInput1.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = SpectrumAnalyzer.fileChooser.showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					inputLabel1.setText(SpectrumAnalyzer.fileChooser.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		input1Panel.add(selectInput1);
		input1Panel.add(inputLabel1);
		
		// panel input1
		JPanel input2Panel = new JPanel(new FlowLayout());
		final JLabel inputLabel2 = new JLabel(SpecAnConstants.NO_FILE_SELECTED);
		JButton selectInput2 = new JButton(SpecAnConstants.SELECT_INPUT_FILE);
		selectInput2.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				int result = fc.showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					inputLabel2.setText(fc.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		input2Panel.add(selectInput2);
		input2Panel.add(inputLabel2);
				
		// panel outputfile
		JPanel outputDataPanel = new JPanel(new FlowLayout());
		final JLabel outputFileLabel = new JLabel(SpecAnConstants.NO_FILE_SELECTED);
		JButton selectOutputButton = new JButton(SpecAnConstants.SELECT_OUTPUT_FILE);
		selectOutputButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				int result = fc.showSaveDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					outputFileLabel.setText(fc.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		outputDataPanel.add(selectOutputButton);
		outputDataPanel.add(outputFileLabel);
		
		// panel mixer mode
		JPanel mixerModePanel = new JPanel(new FlowLayout());
		JLabel mixerModeLabel = new JLabel("Mixer Mode: ");
		final JRadioButton radioSum = new JRadioButton("Sum");
		final JRadioButton radioAvg = new JRadioButton("Average");
		ButtonGroup bg = new ButtonGroup();
		bg.add(radioAvg);
		bg.add(radioSum);		
		bg.setSelected(radioAvg.getModel(), true);
		mixerModePanel.add(mixerModeLabel);
		mixerModePanel.add(radioAvg);
		mixerModePanel.add(radioSum);	
				
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(ROW_COUNT, 1));
		mainPanel.add(input1Panel);
		mainPanel.add(input2Panel);
		mainPanel.add(outputDataPanel);
		mainPanel.add(mixerModePanel);
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {				
				String input1 = inputLabel1.getText();
				String input2 = inputLabel2.getText();
				String output = outputFileLabel.getText();
				
				if (!input1.equals(SpecAnConstants.NO_FILE_SELECTED) && !input2.equals(SpecAnConstants.NO_FILE_SELECTED) && !output.equals(SpecAnConstants.NO_FILE_SELECTED)) {
					try {
						WaveFile wave1 = SpectrumAnalyzer.readWaveFile(input1);		
						if (wave1 != null) {																		
							WaveFile wave2 = SpectrumAnalyzer.readWaveFile(input2);	
							if (wave2 != null) {
								short[] fileData1 = SharedDSPFunctions.extractWaveFileData(wave1);
								short[] fileData2 = SharedDSPFunctions.extractWaveFileData(wave2);								
								
								MixerMode mode;
								if (radioAvg.isSelected()) {
									mode = MixerMode.AVERAGE;
								} else {
									mode = MixerMode.SUM;
								}
								AudioMixer.mix(fileData1, fileData2, mode, output);
							}
						}
					} catch (IOException e1) {						
						JOptionPane.showMessageDialog(frame, SpecAnConstants.ERROR_FILE_LOAD);
					} 
					frame.dispose();
				} 																
			}
		});
				
		// Add to frame
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
