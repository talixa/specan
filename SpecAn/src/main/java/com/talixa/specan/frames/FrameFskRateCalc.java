package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.specan.SpectrumAnalyzer;
import com.talixa.specan.dsp.FskBaudRateCalculator;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameFskRateCalc {

	private static final int ROW_COUNT = 2;
	private static final int WIDTH = SpecAnConstants.DEFAULT_FRAME_WIDTH/2;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * (ROW_COUNT+1);
		
	public static void createAndShowGUI(final String inputFile) {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_RATE_CALC);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(frame);	
			
		// low freq panel
		JPanel lowFreqPanel = new JPanel(new FlowLayout());
		lowFreqPanel.add(new JLabel("Low Freq"));
		final JTextField lowFreqText = new JTextField(10);
		lowFreqPanel.add(lowFreqText);
		
		// high freq panel
		JPanel highFreqPanel = new JPanel(new FlowLayout());
		highFreqPanel.add(new JLabel("High Freq"));
		final JTextField highFreqText = new JTextField(10);
		highFreqPanel.add(highFreqText);
		
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(ROW_COUNT, 1));
		mainPanel.add(lowFreqPanel);
		mainPanel.add(highFreqPanel);
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int low = Integer.parseInt(lowFreqText.getText());
				int high = Integer.parseInt(highFreqText.getText());
				float rate = FskBaudRateCalculator.estimateBaudRate(low, high, inputFile);
				SpectrumAnalyzer.showInfoMessage("Baud Rate", "Estimated baud rate: " + rate);						
				frame.dispose();
			}
		});
				
		// Add to frame
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
