package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import com.talixa.specan.listeners.ExitActionListener;
import com.talixa.specan.shared.IconHelper;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameDecodedMessage {
	
	private static final int WIDTH = 550;
	private static final int HEIGHT = 400;	
	
	public static void createAndShowGUI(String message) {
		// Create frame
		JFrame frame = new JFrame(SpecAnConstants.TITLE_DECODED_MSG);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(frame);	
	
		// create components
		JTextArea text = new JTextArea(message);
		text.setLineWrap(true);
		JButton ok = new JButton(SpecAnConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(frame));
				
		// Add to frame
		frame.add(text, BorderLayout.CENTER);
		frame.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}	
}
