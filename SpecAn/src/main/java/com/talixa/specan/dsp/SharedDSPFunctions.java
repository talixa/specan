package com.talixa.specan.dsp;

import com.talixa.audio.wav.WaveFile;

public class SharedDSPFunctions {
	
	public static double SAMPLE_RATE = 8000;

	public static byte[] shortArrayToByteArray(short[] shortArray) {		
		byte[] byteData = new byte[shortArray.length*2];
		for(int i = 0; i < shortArray.length; ++i) {
			byteData[i*2+1] = (byte)(shortArray[i] >>> 8 & 0xff);
			byteData[i*2] = (byte)(shortArray[i] & 0xff);
		}
		return byteData;
	}
	
	public static short[] extractWaveFileData(WaveFile waveFile) {
		short[] data = new short[waveFile.getNumberOfSamples()];
		for(int i = 0; i < data.length; ++i) {
			data[i] = (short)waveFile.getSampleAt(i*2);
		}
		return data;
	}
	
	public static int getFreqBySampleNumber(int fftLength, int sampleNumber) {	
		return (int)((SAMPLE_RATE/fftLength) * sampleNumber);		
	}
	
	public static int getSampleNumberByFrequency(int fftLength, double freq) {
		return (int)Math.pow((SAMPLE_RATE/(fftLength*freq)),-1);
	}
}
