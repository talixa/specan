package com.talixa.specan.dsp;

import java.io.IOException;

import com.talixa.audio.wav.WaveFile;
import com.talixa.audio.wav.WaveGenerator;

public class GainController {
	
	/*
	 * To change gain, multiply sample by gain
	 */
	
	// input and output are PCM samples
	public static short[] setGain(short[] data, double gain) {
		short[] newData = new short[data.length];
		for(int i = 0; i < data.length; ++i) {
			newData[i] = (short)(data[i] * gain);
		}
		return newData;
	}		
	
	public static void setGain(short[] data, double gain, String outputFile) throws IOException {
		short[] newData = setGain(data,gain);
		byte[] byteData = SharedDSPFunctions.shortArrayToByteArray(newData);		
		WaveFile w = WaveGenerator.generateWaveFromRaw16bitPcm(byteData);		
		w.outputToFile(outputFile);		
	}		
}
