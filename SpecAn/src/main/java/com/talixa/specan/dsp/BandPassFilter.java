package com.talixa.specan.dsp;

import java.io.IOException;

import com.talixa.audio.wav.WaveFile;
import com.talixa.audio.wav.WaveGenerator;
import com.talixa.specan.fft.Complex;
import com.talixa.specan.fft.FFT;

public class BandPassFilter {

	/*
	 * To make a band pass filter:
	 * 1) Run FFT on input data
	 * 2) Zero all data above & below the pass range
	 * 3) DeFFT the data
	 * 4) Output
	 */
	
	// TODO running the frequency translator twice resolved issues with amplitude. Can I do the same here?
	private static final int FFT_LEN = 8192*2;
	private static final int SHIFT_VALUE = FFT_LEN;	
			
	public static short[] filterData(short[] data, int filterBelowFreq, int filterAboveFreq) {
		int numberSamples = data.length;
		short[] filteredData = new short[numberSamples];
		Complex[] amplitudeData = new Complex[FFT_LEN];
		Complex[] frequencyData;	
		
		for(int baseAddress = 0; (baseAddress)+FFT_LEN < numberSamples; baseAddress+=SHIFT_VALUE) {			
			// fill the data array
			for (int offset = 0; offset < FFT_LEN; offset++) {
				int desiredSampleIndex = (baseAddress)+offset;					
				short signedSample = data[desiredSampleIndex];									
				double sample = ((double) signedSample) / (Short.MAX_VALUE);
				amplitudeData[offset] = new Complex(sample,0);
			}															

			// run fft
			frequencyData = FFT.fft(amplitudeData);
			
			// remove below low freq
			int removeBelow = SharedDSPFunctions.getSampleNumberByFrequency(FFT_LEN, filterBelowFreq);
			for(int i = 0; i < removeBelow; ++i) {
				frequencyData[i] = new Complex(0,0);
				frequencyData[FFT_LEN-i-1] = new Complex(0,0);
			}
			
			// remove above high freq			
			int removeAbove = SharedDSPFunctions.getSampleNumberByFrequency(FFT_LEN, filterAboveFreq);
			for(int i = removeAbove; i < frequencyData.length/2; ++i) {
				frequencyData[i] = new Complex(0,0);
				frequencyData[FFT_LEN-i-1] = new Complex(0,0);
			}									
			
			// undo fft
			Complex[] filterFft = FFT.ifft(frequencyData);
			
			// create new data array
			for(int i = 0; i < FFT_LEN; ++i) {					
				filteredData[baseAddress+i] = (short)(filterFft[i].re() * Short.MAX_VALUE);				
			}						
		}
				
		return filteredData;	
	}	
	
	public static void filterData(short[] data, int lowFreq, int highFreq, String outputFile) throws IOException {								
		short[] filteredData = filterData(data, lowFreq, highFreq);						
		byte[] byteData = SharedDSPFunctions.shortArrayToByteArray(filteredData);										
		WaveFile out = WaveGenerator.generateWaveFromRaw16bitPcm(byteData);
		out.outputToFile(outputFile);		
	}
}
