package com.talixa.specan.dsp;

import java.io.IOException;

import com.talixa.audio.wav.WaveFile;
import com.talixa.audio.wav.WaveGenerator;

public class ToneGenerator {

	/*
	 * To generate tone
	 * 1) Calculate angle of wave
	 * 2) Convert angle to amplitude 
	 */
				
	public static short[] generateWave(double frequency, double volume, int seconds, int phase) {
		if (volume > 1) {
			volume = 1;
		}
		
		double samplesPerWave = (SharedDSPFunctions.SAMPLE_RATE / frequency);
				
		short[] data = new short[seconds * (int)SharedDSPFunctions.SAMPLE_RATE];
		for(int i = 0; i < data.length; ++i) {
			// calculate angle of wave
			double angle = 0;
			if (i != 0) {
				angle = 360f/samplesPerWave*i;
			}
			// calculate sine of wave
			double radians = Math.toRadians(angle+phase);
			double sine = Math.sin(radians);
			// create sample @ desired volume
			data[i] = (short)(sine*(Short.MAX_VALUE*volume));
		}
		return data;
	}
	
	public static void generateWave(double frequency, double volume, int length, int phase, String outputFile) throws IOException {
		short[] data = generateWave(frequency, volume, length, phase);
		byte[] byteData = SharedDSPFunctions.shortArrayToByteArray(data);		
		WaveFile w = WaveGenerator.generateWaveFromRaw16bitPcm(byteData);		
		w.outputToFile(outputFile);		
	}
	
	public static short[] generateWave(double frequency, double volume, int length) {
		return generateWave(frequency,volume,length,0);
	}
	
	public static void generateWave(double frequency, double volume, int length, String outputFile) throws IOException {
		generateWave(frequency, volume, length, 0, outputFile);
	}
}
