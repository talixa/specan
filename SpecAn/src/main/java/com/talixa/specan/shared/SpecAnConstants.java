package com.talixa.specan.shared;

public class SpecAnConstants {
	public static final String APPNAME = "Signal Analyzer";
	public static final String VERSION = "1.6";
	
	public static final int HEIGHT_PER_PANEL = 45;
	public static final int DEFAULT_FRAME_WIDTH = 500;
	
	public static final String ICON = "res/specan.png";
	
	// Using images from: http://www.oracle.com/technetwork/java/index-138612.html
	public static final String ICON_ZOOMIN = "res/zoomin.gif";
	public static final String ICON_ZOOMOUT = "res/zoomout.gif";
	public static final String ICON_PLAY = "res/play.gif";
	public static final String ICON_PAUSE = "res/pause.gif";
	public static final String ICON_IMG = "res/img.gif";
	public static final String ICON_LOOP = "res/loop.gif";
	public static final String ICON_AUDIO = "res/audio.gif";
	
	public static final String TITLE_MAIN = APPNAME + " " + VERSION;
	public static final String TITLE_ABOUT = "About " + APPNAME;
	public static final String TITLE_SETTINGS = APPNAME + " Settings";	
	public static final String TITLE_DEMOD_FSK = "RTTY Demod Options";
	public static final String TITLE_DEMOD_PSK = "PSK31 Demod Options";
	public static final String TITLE_DEMOD_CW = "CW Decode Options";
	public static final String TITLE_DECODED_MSG = "Decoded Message";
	public static final String TITLE_TONE_GENERATOR = "Tone Generator";
	public static final String TITLE_GAIN_CONTROLLER = "Gain Controller";
	public static final String TITLE_MIXER = "Audio Mixer";
	public static final String TITLE_FILTER = "Band Pass Filter";
	public static final String TITLE_FREQUENCY_TRANSLATION = "Frequency Translation";
	public static final String TITLE_RATE_CALC = "RTTY Baud Rate Calculator";
	
	// file menu
	public static final String MENU_FILE = "File";
	public static final String MENU_LINE_IN = "Line In";
	public static final String MENU_OPEN = "Open";
	public static final String MENU_EXIT = "Exit";
	
	// demod menu
	public static final String MENU_DEMOD = "Demod";
	public static final String MENU_RTTY = "RTTY";
	public static final String MENU_PSK = "PSK31";
	public static final String MENU_OOK = "CW";
	public static final String MENU_DTMF = "DTMF";
	
	// dsp menus
	public static final String MENU_FUNCTIONS = "Functions";
	public static final String MENU_TOOLS = "Tools";
	public static final String MENU_GENERATOR = "Tone Generator";
	public static final String MENU_MIXER = "Audio Mixer";
	public static final String MENU_GAIN = "Gain Controller";
	public static final String MENU_FILTER = "Band Pass Filter";
	public static final String MENU_SHIFT = "Frequency Shift";
	public static final String MENU_RTTY_CALC = "RTTY Baud Rate";
	
	// help menu
	public static final String MENU_HELP = "Help";	
	public static final String MENU_ABOUT = "About";		
	
	public static final String LABEL_OK = "Ok";
	public static final String LABEL_CANCEL = "Cancel";
	public static final String NO_FILE_SELECTED = "No File Selected";
	public static final String SELECT_OUTPUT_FILE = "Select Output File";
	public static final String SELECT_INPUT_FILE = "Select Input File";
	
	public static final int APP_WIDTH = 530;
	public static final int APP_HEIGHT = 635;	// height of spectrum + height of menu + toolbar + label
	
	public static final String ERROR_TITLE = "Error";
	public static final String ERROR_FILE_LOAD = "Error loading file";
	public static final String ERROR_BAD_WAVE = "Corrupt wave file";	
	
	// a flag to determine if demods are running in gui mode - default is true
	public static boolean isGui = true;
}
