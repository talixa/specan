package com.talixa.specan.shared;

public class SpecAnLogger {

	private static boolean debug = false;
		
	public static void debug(String msg) {
		if (debug) {			
			System.out.println(msg);
		}
	}
	
	public static void debug() {
		if (debug) {			
			System.out.println();
		}
	}
	
	public static void debugBit(int bit) {
		if (debug) {
			System.out.print(bit);
		}
	}
	
	public static void enableLogging(boolean enabled) {
		debug = enabled;
	}
}
