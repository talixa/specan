package com.talixa.specan.shared;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.ImageIcon;

public class IconHelper {

	private static ClassLoader cl = IconHelper.class.getClassLoader();	
	
	public static void setIcon(Window w) {
		Image im = Toolkit.getDefaultToolkit().getImage(cl.getResource(SpecAnConstants.ICON));
		w.setIconImage(im);
	}
	
	public static ImageIcon getImageIcon(String resource) {
		return new ImageIcon(cl.getResource(resource));
	}
}
