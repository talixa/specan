package com.talixa.specan.fft;

public class FrequencyTools {

    public static double getFrequencyIntensity(double re, double im) {
    	return Math.sqrt((re*re)+(im*im));
    }
    
    public static double magSquared(double re, double im) {
    	return (re*re+im*im);
    }
    
    public static double decibels(double re,double im) {
    	return ((re == 0 && im == 0) ? (0) :  10.0 * Math.log10((double)(magSquared(re,im)))) ;
    }
        
    public static double amplitude(double re,double im,int len) {
    	return (getFrequencyIntensity(re,im)/(len));
    }
    
    public static double amplitudeScaled(double re,double im,int len,int scale) { 
    	return amplitude(re,im,len)%scale;
    }  
}
