package com.talixa.specan.fft;

public class WaveFormTests {

	private static final int FLAT_TOP        = 32124;		// max value of any point
	
	/*
	 * Find the point of maximum deviation between start and end points of data
	 */
	public static int getMaxDeviationPoint(short[] absArray, int start, int end) {
		// max height of wave
		int maxDeviation = 0;
		int maxDeviationPoint = 0;
		for(int index = start; index <= end; ++index) {						
			if (absArray[index] > maxDeviation) {
				maxDeviation = absArray[index];
				maxDeviationPoint = index;
			}
		}
		
		// check for flat top wave... must not allow the first instance to be the 'middle'
		if (maxDeviation == FLAT_TOP) {
			int flatTopSize = 1;
			// find the last part of flat top
			for(int i = maxDeviationPoint+1; i < end; ++i) {				
				if (absArray[i] == FLAT_TOP) {
					++flatTopSize;
				} else {
					break;
				}
			}
			
			// reset maxindex to half way between begining and end of flat top
			maxDeviationPoint = (flatTopSize / 2) + maxDeviationPoint;
		}
		
		return maxDeviationPoint;
	}
	
	public static int getAverageOfPoints(short[] absArray, int start, int end) {
		double avg = 0;
		for(int i = start; i < end; ++i) {
			avg += absArray[i];
		}
		avg = avg/(end-start);
		
		return (int)avg;
	}
	
	public static boolean isGenerallyAscending(short[] absArray, int start, int end) {		
		int numAscending = 0;
		int numWrong     = 0;
		
		for(int i = start; i < end-1 ; ++i) {
			if (absArray[i] <= absArray[i+1]) {
				++numAscending;			
			} else {
				++numWrong;
			}
		}		
		return numWrong * 2 < numAscending;
	}
	
	public static boolean isGenerallyDecending(short[] absArray, int start, int end) {		
		int numDecending = 0;
		int numWrong     = 0;
		
		for(int i = start; i < end-1; ++i) {
			if (absArray[i] >= absArray[i+1]) {
				++numDecending;			
			} else {
				++numWrong;
			}
		}
		
		return numWrong * 2 <= numDecending;
	}
}
