package com.talixa.specan;

import com.talixa.specan.shared.SpecAnConstants;
import com.talixa.specan.shared.SpecAnLogger;

public class TestFunctions {

	public static void setupTesting() {
		SpecAnConstants.isGui = false;
		SpecAnLogger.enableLogging(false);
	}
	
	public static void printHeader(Class clazz) {
		SpecAnLogger.debug("*********************************************************");
		SpecAnLogger.debug("Testing " + clazz.getSimpleName());
	}
	
	public static void printFile(String fileName) {
		SpecAnLogger.debug("Input file is: " + fileName);
	}
	
	public static void printOutput(String data) {
		SpecAnLogger.debug("Output data is: " + data);
	}
}
