package com.talixa.specan.demod.fsk;

import org.junit.Test;

import com.talixa.specan.TestFunctions;

import junit.framework.Assert;

public class RttyDemodTest {

	private static final String[] TEST_FILES = {"res/rtty-170-45.wav", "res/rtty-170-50.wav", "res/rtty-425-50.wav", "res/rtty-850-50.wav"};
	private static final int[]    TEST_LOW  = { 920,  930,  800,  585};
	private static final int[]    TEST_HIGH = {1090, 1100, 1230, 1430};
	private static final double[] TEST_BAUD = {45.45, 50, 50, 50};
	
	private static final String DEMO_TEXT = "??? THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG 1234567890";
	
	@Test
	public void testRttyDemod() {
		TestFunctions.setupTesting();
		TestFunctions.printHeader(RttyDemod.class);
		for(int i = 0; i < TEST_FILES.length; ++i) {
			try {
				TestFunctions.printFile(TEST_FILES[i]);
				RttyDemod demod = new RttyDemod(TEST_FILES[i]);
				String data = demod.demodulateBaudot(TEST_LOW[i], TEST_HIGH[i], TEST_BAUD[i]);
				TestFunctions.printOutput(data);
				Assert.assertTrue(data.startsWith(DEMO_TEXT));	
			} catch (Exception e) {
				Assert.fail(e.getMessage());
			}
		}	
	}
}
