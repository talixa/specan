package com.talixa.specan.demod.psk;

import org.junit.Assert;
import org.junit.Test;

import com.talixa.audio.wav.WaveFile;
import com.talixa.audio.wav.WaveReader;
import com.talixa.specan.TestFunctions;
import com.talixa.specan.dsp.SharedDSPFunctions;

public class Psk31DemodTest {

	private static final String[] TEST_FILES = {"res/psk31-1900hz.wav", "res/SK01-061228-BPSK.wav"};
	private static final int[] CENTER_FREQS = {1900, 1023};	
	
	private static final String[] SAMPLE_TEXT = {
			"Antenna is hanging from the ceiling upstairs in my house",
			"60566 72540 52116 86043 22252 73082 57167 84370 87754 61884"
	};
	
	@Test
	public void testPskDemod() {
		TestFunctions.setupTesting();
		TestFunctions.printHeader(Psk31Demod.class);
		for(int i = 0; i < TEST_FILES.length; ++i) {
			try {
				TestFunctions.printFile(TEST_FILES[i]);
				WaveFile wave = WaveReader.readFromFile(TEST_FILES[i]);
				short[] data = SharedDSPFunctions.extractWaveFileData(wave);
				Psk31Demod demod = new Psk31Demod(data);
				String demodData = demod.demodulate(CENTER_FREQS[i]);	
				TestFunctions.printOutput(demodData);
				Assert.assertTrue(demodData.contains(SAMPLE_TEXT[i]));
			} catch (Exception e) {
				Assert.fail(e.getMessage());
			}
		}
	}
}
