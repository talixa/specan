package com.talixa.specan.demod.dtmf;

import org.junit.Test;

import com.talixa.audio.wav.WaveFile;
import com.talixa.audio.wav.WaveReader;
import com.talixa.specan.TestFunctions;
import com.talixa.specan.dsp.SharedDSPFunctions;

import junit.framework.Assert;

public class DTMFDecoderTest {

	private static final String DTMF = "res/dtmf.wav";
	private static final String TONES = "01149806138350691292566990114980613835069129256699";
	
	@Test
	public void testDecode() {
		TestFunctions.setupTesting();
		TestFunctions.printHeader(DTMFDecoder.class);
		try {	
			TestFunctions.printFile(DTMF);
			WaveFile waveFile = WaveReader.readFromFile(DTMF);
			short[] data = SharedDSPFunctions.extractWaveFileData(waveFile);	
			String tones = DTMFDecoder.detect(data);
			TestFunctions.printOutput(tones);
			Assert.assertEquals(TONES, tones);
		} catch (Exception e) {			
			Assert.fail(e.getMessage());
		}
	}
}
