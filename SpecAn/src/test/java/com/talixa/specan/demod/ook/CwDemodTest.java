package com.talixa.specan.demod.ook;

import org.junit.Test;

import com.talixa.specan.TestFunctions;

import junit.framework.Assert;

public class CwDemodTest {

	private static final String[] TEST_FILES = {"res/cw-5wpm.wav"};
	private static final String DEMO_TEXT = "quick brown fox";
	
	@Test
	public void testCwDemod() {
		TestFunctions.setupTesting();
		TestFunctions.printHeader(CwDemod.class);
		try {
			TestFunctions.printFile(TEST_FILES[0]);
			CwDemod demod = new CwDemod(TEST_FILES[0]);
			String data = demod.demodulate();
			TestFunctions.printOutput(data);
			Assert.assertTrue(data.contains(DEMO_TEXT));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
